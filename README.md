Assembler
=========
This is a multi platform assembly wrapper. It can handle Illumina (single- and
paired-end), LS454 (single-, paired- and mate-paired-end), and Ion Torrent
(single-end).


Installation Via Docker
=======================
git clone https://bitbucket.org/genomicepidemiology/assembler assembler
cd assembler
docker build -t assembler .


Usage Examples
==============
### Illumina Paired-end ###
```
#!bash
docker run --rm -v `pwd`:/workdir \
       assembler velvet --shortPaired fastq test/ipe_R1.fq.gz test/ipe_R2.fq.gz \
       --add_velvetg '"'"'-very_clean yes'"'"'
```

You can verify the test by checking the md5 sum of the resulting contigs.fa file:
```
#!bash
docker run --rm -v `pwd`:/workdir --entrypoint md5sum assembler -c test/ipe.md5 
```
output/assembly/contigs.fa: OK


### Ion Torrent ###
```
#!bash
docker run --rm -v `pwd`:/workdir \
       assembler newbler --datatype IonTorrent --se test/itse.fq.gz
```

You can verify the test by checking if the resulting contigs file exists:
ls -1 output/denovo/assembly/454LargeContigs.fna


### LS454 ###
```
#!bash
docker run --rm -v `pwd`:/workdir \
       assembler newbler --datatype 454 --se test/ls454se.fq.gz
```

You can verify the test by checking if the resulting contigs file exists:
ls -1 output/denovo/assembly/454LargeContigs.fna


Debugging
=========
```
#!bash
docker run --rm -ti -v `pwd`:/workdir --entrypoint /bin/bash assembler
/usr/src/assembler/scripts/denovo_velvet.py --m 7gb --ksizes auto --log info --outpath assembly --shortPaired fastq /workdir/test/ipe_R1.fq.gz /workdir/test/ipe_R2.fq.gz --sfile None --add_velvetg ""'-very_clean yes'"" --exp_cov auto --n 4 --min_contig_lgth 100 --sample None
/usr/src/assembler/scripts/denovo_newbler.py --sample None --sfile None --datatype IonTorrent --m 7gb --n 4 --outpath denovo --se /workdir/ion_reads_small.fq.gz --log info
```


Usage
=====
```
positional arguments:
    velvet               Run velvet denovo assembly
    newbler              Run newbler denovo assembly

optional arguments:
  -h, --help             show this help message and exit
```

### Usage Velvet ###
```
usage: wrapper_denovo_2.0.py velvet [options]

Run Velvet denovo assembly, add_velvetg/add_velveth has to be added with quotes, eg: add_velvetg "-very_clean
yes". NB: Trimming does not work on already interleaved files, This version of the wrapper only takes fasta
and fastq as input Default ksizes (auto) will run a range awer45 (read_length/3..read_length/3*2) with step 4
between

optional arguments:
  -h, --help                                      show this help message and exit
  --sample SAMPLE                                 name of run and output directory [run]
  --n N                                           number of threads for parallel run [4]
  --m M                                           memory needed for assembly [7gb]
  --host HOST                                     submit jobs from another host through ssh
  --log LOG                                       log level [INFO]
  --wait                                          wait for assembly, ONLY www [False]
  --short SHORT [SHORT ...]                       short reads
  --shortPaired SHORTPAIRED [SHORTPAIRED ...]     short paired reads
  --short2 SHORT2 [SHORT2 ...]                    short2 reads
  --shortPaired2 SHORTPAIRED2 [SHORTPAIRED2 ...]  short paired2 reads
  --long LONG [LONG ...]                          long reads
  --longPaired LONGPAIRED [LONGPAIRED ...]        long paired reads
  --ksizes KSIZES [KSIZES ...]                    kmers to run assemblies for (single (m) or m M s (min, max,
                                                  step)) [auto]
  --outpath OUTPATH                               name of run, also output dir [assembly]
  --trim                                          should input files be trimmed (illumina only) [False]
  --min_contig_lgth MIN_CONTIG_LGTH               mininum length to report contig [100]
  --cov_cutoff COV_CUTOFF                         coverage cutoff for removal of low coverage (float) [None]
  --exp_cov EXP_COV                               Expected mean coverage (None, float, auto) [auto]
  --ins_length INS_LENGTH                         insert size (reads included) [None]
  --add_velveth ADD_VELVETH                       additional parameters to velveth
  --add_velvetg ADD_VELVETG                       additional parameters to velvetg
```

### Usage newbler ###
```
usage: wrapper_denovo_2.0.py newbler [options]

Run Newbler (2.6) denovo assembly. Format can be: fasta, fastq, sff. If both fasta and qual files should be
used for assembly only add the fasta file and make sure the qual file is in the same path with same name
except .qual ending

optional arguments:
  -h, --help           show this help message and exit
  --sample SAMPLE      name of run and output directory [run]
  --n N                number of threads for parallel run [4]
  --m M                memory needed for assembly [7gb]
  --host HOST          submit jobs from another host through ssh
  --torque             Set this option to use the TORQUE queing manager [False]
  --q Q                queue to submit jobs to [None]
  --log LOG            log level [INFO]
  --wait               wait for assembly, ONLY www [False]
  --se SE [SE ...]     input sff/fasta/fastq files
  --pe PE [PE ...]     input paired end sff/fasta/fastq files
  --outpath OUTPATH    assembly output dir [denovo]
  --datatype DATATYPE  type of data (454/IonTorrent)
```


License
=======
Copyright 2016 Center for Genomic Epidemiology, Technical University of Denmark

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
