#!/usr/bin/env python3
import argparse
import gzip
import subprocess
import sys

def required_nargs(min,max):
   '''Enforces input to nargs to be between min and max long'''
   class RequiredInterval(argparse.Action):
      def __call__(self, parser, args, value, option_string=None):
         if not min<=len(value)<=max:
            msg='argument "{f}" requires between {min} and {max} arguments'.format(
               f=self.dest,min=min,max=max)
            raise argparse.ArgumentTypeError(msg)
         setattr(args, self.dest, value)
   return RequiredInterval

# should be run on trimmed data(!)
def set_kmersizes(args, no_reads=10000, step=4):
   '''Automatically sets kmersizes from avg read lengths'''

   from Bio.SeqIO.QualityIO import FastqGeneralIterator
   from Bio import SeqIO
   import sys

   def average(values):
      '''Computes the arithmetic mean of a list of numbers.'''
      return sum(values, 0.0) / len(values)

   def get_readlengths(f, no_reads):
      '''Get avg. readlengths of no_reads'''

      def get_filetype(f):
         '''Return filetype (fasta/fastq)'''

         if f.endswith('.gz'):
            inhandle = gzip.open(f, 'rt')
         else:
            inhandle = open(f, "rt")

         line = inhandle.readline()
         if line.startswith(">"):
            out = 'fasta'
         elif line.startswith("@"):
            out = 'fastq'
         else:
            raise ValueError('Input must be fasta or fastq')
         return out

      # main
      if f.endswith('.gz'):
         fh = gzip.open(f, 'rt')
      else:
         fh = open(f, 'rt')

      count = 0
      L = []
      if get_filetype(f) == 'fastq':
         for (title, sequence, quality) in FastqGeneralIterator(fh):
            if count < no_reads:
               L.append(len(sequence))
               count = count + 1
            else:
               return average(L)
         # if not returned yet (less than no_reads in the file return now)
         return average(L)
      elif get_filetype(f) == 'fasta':
         for rec in SeqIO.parse(fh, 'fasta'):
            if count < no_reads:
               L.append(len(rec.seq))
               count = count + 1
            else:
               return average(L)
         # if not returned yet (less than no_reads in the file return now)
         return average(L)
      else:
         raise ValueError('Input must be fasta or fastq')

   def floor_to_odd(number):
      number = int(number)
      if number%2==0:
         return number - 1
      else:
         return number

   # main
   files = []
   if args.short: files.extend(args.short)
   if args.short2: files.extend(args.short2)
   if args.shortPaired: files.extend(args.shortPaired)
   if args.shortPaired2: files.extend(args.shortPaired2)

   # remove any occurences of fasta, fastq, fastq.gz, fasta.gz
   files = [a for a in files if a not in ['fasta','fastq','fasta.gz','fastq.gz']]
   
   # get average lengths
   avg_lengths = []
   for f in files:
      avg_f = get_readlengths(f, no_reads)
      avg_lengths.append(avg_f)

   # get average of files and set ksizes from this
   avg = average(avg_lengths)
   if avg <= 75: ksizes = [str(floor_to_odd(avg/3*1)), str(floor_to_odd(avg/3*2.5)), '4']
   elif avg <= 100: ksizes = [str(floor_to_odd(avg/3*1.25)), str(floor_to_odd(avg/3*2.25)), '4']
   elif avg > 100: ksizes = [str(floor_to_odd(avg/3*1)), str(floor_to_odd(avg/3*2.25)), '4']

   # change min ksizes if it is smaller than 15. Memory requirements becomes very large at k < 15
   # change max ksizes if larger than 99, only compiled to 99 as max
   if int(ksizes[0]) < 15: ksizes[0] = '15'
   if int(ksizes[1]) > 150: ksizes[1] == '150'
   sys.stderr.write('Ksizes set to (min, max, step) %s\n' % ' '.join(ksizes))

   # check if ksizes are way to large (maybe draft genome was submitted instead of reads)
   if int(ksizes[1]) > 300:
      raise ValueError('Estimated k-mer sizes found to be %s to %s - did you submit a draft genome?\n' % (ksizes[0], ksizes[1]))
   else:
      return ksizes


def illumina_trim(args, min_length, min_baseq, min_avgq, min_adaptor_match, keep_n):
   '''Create single end trim calls'''

   import os
   import stat
   import modules
   paths = modules.setSystem()

   cmd = '%s/illumina_trim_h.py'%(paths['home'])
   calls = []
   if args.short:
      if args.short[0] == 'fastq' or args.short[0] == 'fastq.gz':
         outfiles_short = []
         for i,f in enumerate(args.short):
            if i == 0: continue
            outfile_short = 'trimmed/' + os.path.split(f)[1] + '.trim.fq'
            outfiles_short.append(outfile_short)
            arg = ' --i %s --min_length %i --min_baseq %i --min_avgq %i --min_adaptor_match %i --o %s ' % (f, min_length,
               min_baseq, min_avgq, min_adaptor_match, outfile_short)
            if keep_n: arg = arg + ' --keep_n'
            calls.append(cmd+arg)
         args.short[1:] = outfiles_short

   if args.short2:
      if args.short2[0] == 'fastq' or args.short2[0] == 'fastq.gz':
         outfiles_short2 = []
         for i,f in enumerate(args.short2):
            if i == 0: continue
            outfile_short2 = 'trimmed/' + os.path.split(f)[1] + '.trim.fq'
            outfiles_short2.append(outfile_short2)
            arg = ' --i %s --min_length %i --min_baseq %i --min_avgq %i  --min_adaptor_match %i --o %s ' % (f, min_length,
               min_baseq, min_avgq, min_adaptor_match, outfile_short2)
            if keep_n: arg = arg + ' --keep_n'
            calls.append(cmd+arg)
         args.short2[1:] = outfiles_short2

   if args.shortPaired and args.shortPaired[0].find('fastq') > -1:
      outfiles_shortPaired = []
      if len(args.shortPaired) == 3:
         outfile_pe1 = 'trimmed/' + os.path.split(args.shortPaired[1])[1] + '.trim.fq'
         outfile_pe2 = 'trimmed/' + os.path.split(args.shortPaired[2])[1] + '.trim.fq'
         outfiles_shortPaired.append(outfile_pe1)
         outfiles_shortPaired.append(outfile_pe2)
         arg = ' --i %s %s --min_length %i --min_baseq %i --min_avgq %i --min_adaptor_match %i --o %s %s' % (args.shortPaired[1], args.shortPaired[2], min_length, min_baseq, min_avgq,  min_adaptor_match, outfile_pe1, outfile_pe2)
      elif len(args.shortPaired) == 2:
         outfile_pe1 = 'trimmed/' + os.path.split(args.shortPaired[1])[1] + '_1.trim.fq'
         outfile_pe2 = 'trimmed/' + os.path.split(args.shortPaired[1])[1] + '_2.trim.fq'
         outfiles_shortPaired.append(outfile_pe1)
         outfiles_shortPaired.append(outfile_pe2)
         arg = ' --i %s --min_length %i --min_baseq %i --min_avgq %i --min_adaptor_match %i --o %s %s' % (args.shortPaired[1], min_length, min_baseq, min_avgq,  min_adaptor_match, outfile_pe1, outfile_pe2)
      else:
         raise ValueError('Length of input to shortPaired is not correct')

      if keep_n: arg = arg + ' --keep_n'
      calls.append(cmd+arg)
      args.shortPaired[1:] = outfiles_shortPaired

   if args.shortPaired2 and args.shortPaired[0].find('fastq') > -1:
      outfiles_shortPaired2 = []
      if len(args.shortPaired2) == 3:
         outfile_pe1 = 'trimmed/' + os.path.split(args.shortPaired2[1])[1] + '.trim.fq'
         outfile_pe2 = 'trimmed/' + os.path.split(args.shortPaired2[2])[1] + '.trim.fq'
         outfiles_shortPaired2.append(outfile_pe1)
         outfiles_shortPaired2.append(outfile_pe2)
         arg = ' --i %s %s --min_length %i --min_baseq %i --min_avgq %i --min_adaptor_match %i --o %s %s' % (args.shortPaired2[1], args.shortPaired2[2], min_length, min_baseq, min_avgq,  min_adaptor_match, outfile_pe1, outfile_pe2)
      elif len(args.shortPaired2) == 2:
         outfile_pe1 = 'trimmed/' + os.path.split(args.shortPaired2[1])[1] + '_1.trim.fq'
         outfile_pe2 = 'trimmed/' + os.path.split(args.shortPaired2[1])[1] + '_2.trim.fq'
         outfiles_shortPaired2.append(outfile_pe1)
         outfiles_shortPaired2.append(outfile_pe2)
         arg = ' --i %s --min_length %i --min_baseq %i --min_avgq %i --min_adaptor_match %i --o %s %s' % (args.shortPaired2[1], min_length, min_baseq, min_avgq,  min_adaptor_match, outfile_pe1, outfile_pe2)
      else:
         raise ValueError('Length of input to shortPaired2 is not correct')
      if keep_n: arg = arg + ' --keep_n'
      calls.append(cmd+arg)
      args.shortPaired2[1:] = outfiles_shortPaired2

   if len(calls) > 0:
      if not os.path.exists('trimmed'):
         os.makedirs('trimmed')
   
   # COMBINE IN SH-FILES #
   sh_calls = []
   for i in range(len(calls)):
      filename = 'trim%i.sh'%i
      fh = open(filename, 'w')
      fh.write('#!/bin/sh\n\n')
      fh.write(calls[i]+'\n')
      fh.close()
      sh_calls.append(filename)
      # Set execution permission
      st = os.stat(filename)
      os.chmod(filename, st.st_mode | stat.S_IXUSR)
   
   return sh_calls

def create_velvet_calls(args):
   '''Create velvet calls'''
   import os
   import stat
   import modules
   paths = modules.setSystem()
   
   # VELVETH CALLS
   # create calls, outpath, ksizes, format, readtypes, reads
   cmd = '%svelveth' % paths['velvet_home']
   velveth_calls = []
   if len(args.ksizes) == 1:
      arg = ' %s %s -create_binary ' % (args.outpath, args.ksizes[0])
      if args.short: arg = arg + ' -short -fmtAuto %s' % (' '.join(args.short[1:]))
      if args.short2: arg = arg + ' -short2 -fmtAuto %s' % (' '.join(args.short2[1:]))
      if args.shortPaired:
         if len(args.shortPaired) == 2:
            arg = arg + ' -shortPaired -fmtAuto %s' % (args.shortPaired[1])
         elif len(args.shortPaired) == 3:
            arg = arg + ' -shortPaired -separate -fmtAuto %s %s' % (args.shortPaired[1], args.shortPaired[2])
      if args.shortPaired2:
         if len(args.shortPaired2) == 2:
            arg = arg + ' -shortPaired2 -fmtAuto %s' % (args.shortPaired2[1])
         elif len(args.shortPaired) == 3:
            arg = arg + ' -shortPaired2 -separate -fmtAuto %s %s' % (args.shortPaired2[1], args.shortPaired2[2])
      if args.long: arg = arg + ' -long -fmtAuto %s' % (' '.join(args.long[1:]))
      if args.longPaired:
         if len(args.longPaired) == 2:
            arg = arg + ' -longPaired -fmtAuto %s' % (args.longPaired[1])
         elif len(args.longPaired) == 3:
            arg = arg + ' -longPaired -separate -fmtAuto %s %s' % (args.longPaired[1], args.longPaired[2])
      if args.add_velveth: arg = arg + ' %s' % args.add_velveth
      call = cmd + arg
      velveth_calls.append(call)

   elif len(args.ksizes) >= 2 and len(args.ksizes) <= 3:
      if len(args.ksizes) == 2:
         step = 2
      elif len(args.ksizes) == 3:
         step = args.ksizes[2]

      # create calls, outpath, ksizes, format, readtypes, reads
      for k in range(int(args.ksizes[0]), int(args.ksizes[1]), int(step)):
         arg = ' %s_%s %s -create_binary ' % (args.outpath, k, k)
         if args.short: arg = arg + ' -short -fmtAuto %s' % (' '.join(args.short[1:]))
         if args.short2: arg = arg + ' -short2 -fmtAuto %s' % (' '.join(args.short2[1:]))
         if args.shortPaired:
            if len(args.shortPaired) == 2:
               arg = arg + ' -shortPaired -fmtAuto %s' % (args.shortPaired[1])
            elif len(args.shortPaired) == 3:
               arg = arg + ' -shortPaired -separate -fmtAuto %s %s' % (args.shortPaired[1], args.shortPaired[2])
         if args.shortPaired2:
            if len(args.shortPaired2) == 2:
               arg = arg + ' -shortPaired2 -fmtAuto %s' % (args.shortPaired2[1])
            elif len(args.shortPaired) == 3:
               arg = arg + ' -shortPaired2 -separate -fmtAuto %s %s' % (args.shortPaired2[1], args.shortPaired2[2])
         if args.long: arg = arg + ' -long -fmtAuto %s' % (' '.join(args.long[1:]))
         if args.longPaired:
            if len(args.longPaired) == 2:
               arg = arg + ' -longPaired -fmtAuto %s' % (args.longPaired[1])
            elif len(args.longPaired) == 3:
               arg = arg + ' -longPaired -separate -fmtAuto %s %s' % (args.longPaired[1], args.longPaired[2])
         if args.add_velveth: arg = arg + ' %s' % args.add_velveth
         call = cmd + arg
         velveth_calls.append(call)
   else:
      raise ValueError('ksizes must be one value giving ksize, two values giving lower and upper limit (step will be 2) or three values giving lower limit, upper limit and step')

   # VELVETG CALLS
   # create cmd
   cmd = '%svelvetg' % paths['velvet_home']
   cmds = []
   if len(args.ksizes) == 1:
      cmd = '%svelvetg %s' % (paths['velvet_home'], args.outpath)
      cmds.append(cmd)
   elif len(args.ksizes) >= 2 and len(args.ksizes) <= 3:
      if len(args.ksizes) == 2:
         step = 2
      elif len(args.ksizes) == 3:
         step = args.ksizes[2]

      for k in range(int(args.ksizes[0]), int(args.ksizes[1]), int(step)):
         cmd = '%svelvetg %s_%s' % (paths['velvet_home'], args.outpath, k)
         cmds.append(cmd)

   # create arg: cov_cutoff, exp_cov, ins_length, add_velvetg
   velvetg_calls = []
   # add other parameters
   for i in range(len(cmds)):
      arg = ' -min_contig_lgth %i' % args.min_contig_lgth
      if args.cov_cutoff: arg = arg + ' -cov_cutoff %f' % args.cov_cutoff
      if args.exp_cov != "None": arg = arg + ' -exp_cov %s' % args.exp_cov
      if args.ins_length: arg = arg + ' -ins_length %i' % args.ins_length
      if args.add_velvetg: arg = arg + ' %s' % args.add_velvetg
      velvetg_calls.append(cmds[i]+arg)

   # COMBINE IN SH-FILES #
   sh_calls = []
   for i in range(len(velveth_calls)):
      filename = 'velvet%i.sh'%i
      fh = open(filename, 'w')
      fh.write('#!/bin/sh\n\n')
      fh.write(velveth_calls[i]+'\n')
      fh.write(velvetg_calls[i]+'\n')
      fh.close()
      sh_calls.append(filename)
      # Set execution permission
      st = os.stat(filename)
      os.chmod(filename, st.st_mode | stat.S_IXUSR)
   
   return sh_calls


def postprocess(args):
   '''Determine best assembly, remove other assemblies, clean up and write semaphore file (if required)'''
   import os
   import stat
   import modules
   paths = modules.setSystem()
   
   calls = []
   if len(args.ksizes) > 1:
      ## parse_assemblies
      cmd = 'R --vanilla '
      # set argument
      if len(args.ksizes) == 1:
         arg = ' %s %s' % (args.outpath, args.ksizes[0])
      elif len(args.ksizes) >= 2:
         if len(args.ksizes) == 2:
            step = 2
         elif len(args.ksizes) == 3:
            step = args.ksizes[2]
      
         arg_list = []
         for k in range(int(args.ksizes[0]), int(args.ksizes[1]), int(step)):
            out = '%s_%s/stats.txt %s' % (args.outpath, k, k)
            arg_list.append(out)
         arg = ' '.join(arg_list)
      
      call = cmd + arg +' < %s/denovo_velvet_parse.R'%(paths['home'])
      calls.append(call)
      
      ## accept assembly
      call = '%s/denovo_velvet_accept.py %s'%(paths['home'], args.outpath)
      calls.append(call)
   
   ## clean
   call = '%s/denovo_velvet_clean.py'%(paths['home'])
   # calls.append(call)
   
   ## write semaphore
   if args.sfile and args.sfile != 'None':
      sem_dir = os.path.dirname(args.sfile)
      if not os.path.exists(sem_dir):
         os.makedirs(sem_dir)
      calls.append('echo "done" > %s' % args.sfile)
   
   ## write in bash script
   filename = 'postprocess.sh'
   fh = open(filename, 'w')
   fh.write('#!/bin/sh\n\n')
   for call in calls:
      fh.write(call+'\n')
   fh.close()
   # Set execution permission
   st = os.stat(filename)
   os.chmod(filename, st.st_mode | stat.S_IXUSR)
   
   return [filename]


def start_assembly(args, logger):
   '''Start assembly'''
   import modules
   from classes import Moab
   import os
   
   # set queueing
   paths = modules.setSystem()
   home = os.getcwd()
   if args.torque:
     cpuV = 'nodes=1:ppn=%i,mem=%s,walltime=7200' % (args.n, args.m)  # Velvet calls
     cpuA = 'nodes=1:ppn=1,mem=512mb,walltime=1200'                   # Postprocessing call
     cpuF = 'nodes=1:ppn=2,mem=%s,walltime=7200' % args.m             # Trimming call
   
   # set kmersizes (if auto)
   if args.ksizes == ['auto']:
      args.ksizes = set_kmersizes(args)
   
   # trimming calls
   if args.trim:
      trim_calls = illumina_trim(args, int(args.ksizes[0]), 15, 20, 15, False)
      if not os.path.exists('trimmed'):
         os.makedirs('trimmed')
   
   # velvet calls
   velvet_calls = create_velvet_calls(args)
   
   # velvet parse calls
   postprocess_calls = postprocess(args)
   
   if not args.torque:
      # If set, trimming in serial
      if args.trim:
         for call in trim_calls:
            call = "%s/%s"%(home, call)
            subprocess.call(call, shell=True)
      
      # Run the Assembler locally in serial
      for call in velvet_calls:
         call = "%s/%s"%(home, call)
         #sys.stderr.write("calls:%s\n"%calls)
         subprocess.call(call, shell=True)
      
      for call in postprocess_calls:
         call = "%s/%s"%(home, call)
         subprocess.call(call, shell=True)
   
   else:
      # Run the Assembler using TORQUE queue manager
      # set environment variable:
      env_var = 'OMP_NUM_THREADS=%i' % int(args.n)
      
      # submit and release jobs
      print("Submitting jobs")
      
      # if trimming is needed
      if args.trim:
         illuminatrim_moab = Moab(trim_calls, logfile=logger, runname='run_trim', queue=args.q, cpu=cpuF, host=args.host)
         velvet_moab = Moab(velvet_calls, logfile=logger, runname='run_velvet', queue=args.q, cpu=cpuV, depend=True, depend_type='all', depend_val=[1], depend_ids=illuminatrim_moab.ids, env=env_var, host=args.host)
      else:
         velvet_moab = Moab(velvet_calls, logfile=logger, runname='run_velvet', queue=args.q, cpu=cpuV, env=env_var, host=args.host)
      
      # submit job for postprocessing
      postprocess_moab = Moab(postprocess_calls, logfile=logger, runname='run_postprocess', queue=args.q, cpu=cpuA, depend=True, depend_type='conc', depend_val=[len(velvet_calls)], depend_ids=velvet_moab.ids, host=args.host)
      
      # release jobs
      print("Releasing jobs")
      if args.trim and len(trim_calls) > 0: illuminatrim_moab.release()
      velvet_moab.release()
      postprocess_moab.release()
      
      # write out postprocess id to file for Rolf
      fh = open('ppid', 'w')
      fh.write(postprocess_moab.ids[0] + '\n')
      fh.close()


if __name__ == '__main__':
   import os
   import logging
   
   # create the parser
   parser = argparse.ArgumentParser(prog='denovo_velvet.py', formatter_class=lambda prog: argparse.RawDescriptionHelpFormatter(prog,max_help_position=50, width=110), usage='%(prog)s [options]', description='''
   Run Velvet denovo assembly. Read input is given by eg. --short <format> <reads>
   Format can be: fasta, fastq, raw, fasta.gz, fastq.gz, raw.gz, sam, bam
   add_velvetg/add_velveth has to be added with quotes, eg: add_velvetg "-very_clean yes"
   if mate_pairs are used (in eg. shortPaired2) one should add: add_velvetg "-shortMatePaired2 yes -ins_length2 <MP size>"
   NB: Trimming does not work on already interleaved files\n''')
   
   # add the arguments
   parser.add_argument('--short', help='input read format and short reads', nargs='+')
   parser.add_argument('--shortPaired', help='input read format and short paired reads', nargs='+')
   parser.add_argument('--short2', help='input read format and short2 reads', nargs='+')
   parser.add_argument('--shortPaired2', help='input read format and short paired2 reads', nargs='+')
   parser.add_argument('--long', help='input read format and long reads', nargs='+', action=required_nargs(0,3))
   parser.add_argument('--longPaired', help='input read format and long paired reads', nargs='+')
   parser.add_argument('--ksizes', help='kmers to run assemblies for (single (m) or m M s (min, max, step)) [auto]', nargs='+', default=['auto'])
   parser.add_argument('--sample', help='name of run and output directory [velvet_assembly]', default='velvet_assembly')
   parser.add_argument('--outpath', help='assembly output dir [assembly]', default='assembly')
   parser.add_argument('--trim', help='should input files be trimmed (illumina only) [False]', default=False, action='store_true')
   parser.add_argument('--min_contig_lgth', help='mininum length to report contig [100]', default=100, type=int)
   parser.add_argument('--cov_cutoff', help='coverage cutoff for removal of low coverage (float) [None]', default=None, type=float)
   parser.add_argument('--exp_cov', help='Expected mean coverage (None, float, auto) [auto]', default='auto')
   parser.add_argument('--ins_length', help='insert size (reads included) [None]', default=None, type=int)
   parser.add_argument('--add_velveth', help='additional parameters to velveth', default=None)
   parser.add_argument('--add_velvetg', help='additional parameters to velvetg', default=None)
   parser.add_argument('--n', help='number of threads for parallel run [4]', default=4, type=int)
   parser.add_argument('--m', help='memory needed for assembly [2gb]', default='2gb')
   parser.add_argument('--host', help='submit jobs from another host through ssh', default=None)
   parser.add_argument('--torque', help='Set this option to use the TORQUE queing manager [False]', default=False, action='store_true')
   parser.add_argument('--q', help='queue to submit jobs to [None]', default=None)
   parser.add_argument('--log', help='log level [info]', default='info')
   parser.add_argument('--sfile', help='semaphore file for waiting [None]', default=None)
   
   args = parser.parse_args()
   
   #add_velveth and add_velvetg works from commandline, eg:
   #denovo_velvet.py --short fastq.gz interleaved.fastq.gz --ksizes 33 --sample Kleb --add_velvetg "-very_clean yes"
   
   # change to sample dir if set
   if args.sample and args.sample != 'None':
      if not os.path.exists(args.sample):
         os.makedirs(args.sample)
      os.chmod(args.sample, 0o777)
      os.chdir(args.sample)
   
   if not os.path.exists('log'):
      os.makedirs('log')
   
   # start logging
   logger = logging.getLogger('denovo_velvet.py')
   hdlr = logging.FileHandler('denovo_velvet.log')
   formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
   hdlr.setFormatter(formatter)
   logger.addHandler(hdlr)
   if args.log == 'info':
      logger.setLevel(logging.INFO)
   
   # start assembly
   start_assembly(args, logger)
