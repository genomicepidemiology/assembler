#!/usr/bin/env python3
import os, subprocess, argparse
from pipes import quote

def set_abspath():
   '''Returns absolute path of file to argparse'''
   class SetAbspath(argparse.Action):
      def __call__(self, parser, args, filenames, option_string=None):
         import os
         if type(filenames) == str:
            f_abs = os.path.abspath(filenames)
            setattr(args, self.dest, f_abs)
         elif type(filenames) == list:
            new_list = []
            for f in filenames:
               new_list.append(os.path.abspath(f))
            setattr(args, self.dest, new_list)
         else:
            setattr(args, self.dest, filenames)
   return SetAbspath


def newbler(args):
   '''Creating newbler calls'''
   def convert_fastq(args, paths):
      '''If input is fastq convert to fasta+qual'''
      
      cmds = []
      se = []
      pe = []
      # identify file inputs
      if args.se:
         se_ftypes = list(map(modules.set_filetype, args.se))
         for i,f in enumerate(args.se):
            if se_ftypes[i] == 'fastq':
               fasta = os.path.split(f)[1]+'.fasta'
               if f.endswith('.gz'):
                  cmds.append("gzip -cd %s | paste - - - - | cut -f 1,2 | sed 's/^@/>/' | tr '\t' '\n' > %s"%(quote(f),quote(fasta)))
               else:
                  cmds.append("paste - - - - < %s | cut -f 1,2 | sed 's/^@/>/' | tr '\t' '\n' > %s"%(quote(f),quote(fasta)))
               se.append(fasta)
            elif se_ftypes[i] == 'fasta':
               if f.endswith('.gz'):
                  fnew = os.path.splitext(os.path.split(f)[1])[0]
                  cmds.append('''pigz -dc -p %s %s > %s''' % (args.n, f, fnew))
                  se.append(fnew)
                  
                  # look for qual file (dont add to path because newbler will pick it up)
                  possible_qual = os.path.splitext(os.path.splitext(f)[0])[0] + '.qual.gz'
                  if os.path.exists(possible_qual):
                     qnew = os.path.split(os.path.splitext(os.path.splitext(f)[0])[0] + '.qual')[1]
                     cmds.append('''pigz -dc -p %s %s > %s''' % (args.n, possible_qual, qnew))
               else:
                  se.append(f)
            else:
               se.append(f)
      
      if args.pe:   
         pe_ftypes = list(map(modules.set_filetype, args.pe))
         for i,f in enumerate(args.pe):
            if pe_ftypes[i] == 'fastq':
               fasta = os.path.split(f)[1]+'.fasta'
               if f.endswith('.gz'):
                  cmds.append("gzip -cd %s | paste - - - - | cut -f 1,2 | sed 's/^@/>/' | tr '\t' '\n' > %s"%(quote(f),quote(fasta)))
               else:
                  cmds.append("paste - - - - < %s | cut -f 1,2 | sed 's/^@/>/' | tr '\t' '\n' > %s"%(quote(f),quote(fasta)))
               pe.append(fasta)
            elif pe_ftypes[i] == 'fasta':
               if f.endswith('.gz'):
                  fnew = os.path.splitext(os.path.split(f)[1])[0]
                  cmds.append('''pigz -dc -p %s %s > %s''' % (args.n, f, fnew))
                  fnew = os.path.splitext(f)[0]
                  pe.append(fnew)
                  
                  # look for qual file (dont add to path because newbler will pick it up)
                  possible_qual = os.path.splitext(os.path.splitext(f)[0])[0] + '.qual.gz'
                  if os.path.exists(possible_qual):
                     qnew = os.path.split(os.path.splitext(os.path.splitext(f)[0])[0] + '.qual')[1]
                     cmds.append('''pigz -dc -p %s %s > %s''' % (args.n, possible_qual, qnew))
               else:
                  se.append(f)

            else:
               pe.append(f)
      
      return cmds, se, pe
   
   import os
   import stat
   import modules
   paths = modules.setSystem()
   cmds = []
   
   # check if IonTorrent
   if args.datatype == 'IonTorrent':
      cf = convert_fastq(args, paths)
      cmds.extend(cf[0])
      args.se = cf[1]
      args.pe = cf[2]
   elif args.datatype == '454':
      cf = convert_fastq(args, paths)
      cmds.extend(cf[0])
      args.se = cf[1]
      args.pe = cf[2]
   else:
      raise ValueError('--datatype must be 454 or IonTorrent\n')
   
   cmds.append('%snewAssembly %s' % (paths['newbler'], args.outpath))
   if args.se:
      for f in args.se:
         cmds.append('%saddRun -lib shotgun %s %s' % (paths['newbler'], args.outpath, f))
   if args.pe:
      for i,f in enumerate(args.pe):
         cmds.append('%saddRun -p -lib PE%i %s %s' % (paths['newbler'], i, args.outpath, f))
   cmds.append('%srunProject -cpu %s %s' % (paths['newbler'], args.n, args.outpath))
   
   # write in bash script
   filename = 'newbler.sh'
   fh = open(filename, 'w')
   fh.write('#!/bin/sh\n\n')
   
   for cmd in cmds:
      print(cmd)
      fh.write(cmd+'\n')
   fh.close()
   # Set execution permission
   st = os.stat(filename)
   os.chmod(filename, st.st_mode | stat.S_IXUSR)
   
   # return command (NB. add env. variable to run)
   return [filename]


def newbler_stats(args):
   '''Create newbler stats calls'''
   import os
   import stat
   import modules
   paths = modules.setSystem()
   
   cmds = []
   assembly_path = '%s/assembly/' % args.outpath
   cmds.append('''perl -ne 'if ($_ =~ m/^>.+length=(\d+)/) { print $1, "\\n"}' %s > 454AllContigs.lengths ''' % (assembly_path + '454AllContigs.fna'))
   cmds.append('''perl -ne 'if ($_ =~ m/^>.+length=(\d+)/) { print $1, "\\n"}' %s > 454LargeContigs.lengths ''' % (assembly_path + '454LargeContigs.fna'))
   if args.pe:
      cmds.append('''perl -ne 'if ($_ =~ m/^>.+length=(\d+)/) { print $1, "\\n"}' %s > 454Scaffolds.lengths ''' % (assembly_path + '454Scaffolds.fna'))
      cmds.append('R --vanilla 454AllContigs.lengths 454LargeContigs.lengths 454Scaffolds.lengths assembly.stats.txt < %s/denovo_newbler_stats.R '%(paths['home']))

   else:
      cmds.append('R --vanilla 454AllContigs.lengths 454LargeContigs.lengths NA assembly.stats.txt < %s/denovo_newbler_stats.R '%(paths['home']))

   
   ## write semaphore
   if args.sfile and args.sfile != 'None': cmds.append('echo "done" > %s' % args.sfile)   
   
   # write in bash script
   filename = 'newbler_stats.sh'
   fh = open(filename, 'w')
   fh.write('#!/bin/sh\n\n')
   for cmd in cmds:
      fh.write(cmd+'\n')
   fh.close()
   # Set execution permission
   st = os.stat(filename)
   os.chmod(filename, st.st_mode | stat.S_IXUSR)
   
   # return command (NB. add env. variable to run)
   return [filename]

def start_assembly(args, logger):
   '''start newbler assembly'''
   import modules
   from classes import Moab
   import os
   
   # set queueing
   paths = modules.setSystem()
   home = os.getcwd()
   if args.torque:
     cpuV = 'nodes=1:ppn=%i,mem=%s,walltime=42000' % (args.n, args.m)       # Newbler calls
     cpuA = 'nodes=1:ppn=1,mem=512mb,walltime=1200'                         # Postprocessing call
   
   newbler_calls = newbler(args)
   print(newbler_calls)
   newblerstats_calls = newbler_stats(args)
   
   # set environment variable (add newbler binaries to bin):
   env_var = ''
   
   # submit and release jobs
   print("Submitting jobs")
   
   if not args.torque:
      # Run the Assembler locally in serial
      for call in newbler_calls:
         call = "%s/%s"%(home, call)
         subprocess.call(call, shell=True)
      
      for call in newblerstats_calls:
         call = "%s/%s"%(home, call)
         subprocess.call(call, shell=True)
   
   else:
      newbler_moab = Moab(newbler_calls, logfile=logger, runname='run_newbler', queue=args.q, cpu=cpuV, env=env_var, host=args.host)
      newblerstats_moab = Moab(newblerstats_calls, logfile=logger, runname='run_newblerstats', queue=args.q, cpu=cpuA, depend=True, depend_type='one2one', depend_val=[1], depend_ids=newbler_moab.ids, host=args.host)
      
      # release jobs
      print("Releasing jobs")
      newbler_moab.release()
      newblerstats_moab.release()

if __name__ == '__main__':
   import os
   import logging
   
   # create the parser
   parser = argparse.ArgumentParser(prog='denovo_newbler.py', formatter_class=lambda prog: argparse.RawDescriptionHelpFormatter(prog,max_help_position=50, width=110), usage='%(prog)s [options]', description='''
   Run Newbler (2.6) denovo assembly
   Format can be: fasta, fastq, sff
   If both fasta and qual files should be used for assembly only add the fasta file 
   and make sure the qual file is in the same path with same name except .qual ending''')
   
   parser.add_argument('--datatype', help='type of data (454/IonTorrent)', required=True)
   parser.add_argument('--se', help='input sff/fasta/fastq files', nargs='+', action=set_abspath())
   parser.add_argument('--pe', help='input paired end sff/fasta/fastq files', nargs='+', default=None, action=set_abspath())
   parser.add_argument('--sample', help='name of run and output directory [newbler_assembly]', default='newbler_assembly')
   parser.add_argument('--outpath', help='assembly output dir [denovo]', default='denovo')
   parser.add_argument('--n', help='number of cpus for parallel run [4]', default=4, type=int)
   parser.add_argument('--m', help='memory needed for assembly [2gb]', default='2gb')
   parser.add_argument('--host', help='submit jobs from another host through ssh', default=None)
   parser.add_argument('--torque', help='Set this option to use the TORQUE queing manager [False]', default=False, action='store_true')
   parser.add_argument('--q', help='queue to submit jobs to [None]', default=None)
   parser.add_argument('--log', help='log level [info]', default='info')
   parser.add_argument('--sfile', help='semaphore file for waiting [None]', default=None)
   
   args = parser.parse_args()
   
   # change to sample dir if set
   if args.sample and args.sample != 'None':
      if not os.path.exists(args.sample):
         os.makedirs(args.sample)
      os.chmod(args.sample, 0o777)
      os.chdir(args.sample)
   
   if not os.path.exists('log'):
      os.makedirs('log')
   
   # start logging
   logger = logging.getLogger('denovo_newbler.py')
   hdlr = logging.FileHandler('denovo_newbler.log')
   formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
   hdlr.setFormatter(formatter)
   logger.addHandler(hdlr) 
   if args.log == 'info':
      logger.setLevel(logging.INFO)
   
   # start assembly
   start_assembly(args, logger)
