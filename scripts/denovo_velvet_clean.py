#!/usr/bin/env python3
from modules import rm_files
import subprocess
import os

rm_files(['run_velvet.*', 'run_interleave.*', '*.interleaved', 'pbsjob.tmp*', 'run_postprocess.*', 'run_trim.*', 'run_velvet*', '*.sh'])

if os.path.exists('trimmed'):
   subprocess.call('rm -r trimmed/', shell=True)
