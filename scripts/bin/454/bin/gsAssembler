#!/bin/bash
################################################################################
##
##	Filename:	gsAssembler
##
##	Programmer:	Michael S. Braverman 
##
##	$Id: gsAssembler,v 1.38 2010-11-18 14:39:31 jkrupka Exp $
##
################################################################################
##
##	Description:
##
##	Launches the GS Assembly Graphical User Interface on Linux
##
################################################################################

script="$(readlink -f "${BASH_SOURCE:-$0}")"
rootDir="$(dirname "$(dirname "$script")")"
defaultHeapMem=500
defaultPermMem=128
defaultConfigDir="$rootDir"/config
defaultEnableFeatures=
defaultEnableVersion=
defaultEnableVerbose=
defaultEnableHeadless=false
defaultRemoteX=

usage ()
{
    # Note: --enable intentionally not included as part of usage statement.
    #       --remoteX, beta feature, intentionally not included as part of use statement

    echo "Usage: $script [--maxHeap <number>] [--maxPerm <number>] [--configDir <directory>]" >&2
    echo "        --maxHeap   maximum jvm heap space (MB) (default=${defaultHeapMem})" >&2
    echo "        --maxPerm   maximum jvm permgen space (MB) (default=${defaultPermMem})" >&2
    echo "        --configDir application configuration directory (default=${defaultConfigDir})" >&2
    echo "        --version   reports build version and scm tag" >&2
    echo "        --verbose   reports same as --version plus environment details" >&2    
    exit 100
}


heapMem=$defaultHeapMem
permMem=$defaultPermMem
configDir="$defaultConfigDir"
enableFeatures="$defaultEnableFeatures"
enableVersion="$defaultEnableVersion"
enableVerbose="$defaultEnableVerbose"
enableHeadless="$defaultEnableHeadless"
remoteX="$defaultRemoteX"
fourFiveFourVersion=2.6

while [ $# -gt 0 ]
do
  case "$1" in
      --maxHeap)
	  if [ $# -lt 2 ]; then usage; fi
	  heapMem="$2"
	  shift 2
          ;;
      --maxPerm)
	  if [ $# -lt 2 ]; then usage; fi
	  permMem="$2"
	  shift 2
	  ;;
      --configDir)
	  if [ $# -lt 2 ]; then usage; fi
	  configDir="$2"
	  shift 2
	  ;;
      --enable)
	  if [ $# -lt 2 ]; then usage; fi
	  enableFeatures="$2"
	  shift 2
	  ;;
      --version)
      enableVersion="--version"
      enableHeadless=true
      shift
      ;;
      --verbose)
      enableVerbose="--verbose"
      enableHeadless=true
      shift
      ;;
      --remoteX)
	  remoteX=1
	  shift 1
	  ;;
      *)
	  break;
   esac
done

# Only accept --verbose if --version also requested
if [ "$enableVerbose" = "--verbose" ]; then
    if [ "$enableVersion" != "--version" ]; then
       echo "--verbose may only be used with --version" >&2
       usage
    fi
fi

# Only accept a number
echo $heapMem | grep "[^0-9]" > /dev/null 2>&1
if [ "$?" -eq "0" ]; then
    echo "Value '$heapMem' for --maxHeap is not a positive number" >&2
    usage
fi

# Only accept a number
echo $permMem | grep "[^0-9]" > /dev/null 2>&1
if [ "$?" -eq "0" ]; then
    echo "Value '$permMem' for --maxPerm is not a positive number" >&2
    usage
fi


# Only accept a directory
if [ ! -d "$configDir" ]; then
    echo "Value '$configDir' for --configDir is not a directory" >&2
    usage
fi

# jar order and version number is important (see pom.xml for correct versions)
jarFiles=(                                         \
    assembly-$fourFiveFourVersion.jar             \
    analysis-$fourFiveFourVersion.jar              \
    binding-2.0.6.jar                              \
    common-$fourFiveFourVersion.jar                \
    commons-collections-3.2.jar                    \
    commons-logging-1.1.1.jar                      \
    opencsv-2.1.jar                                \
    forms-1.2.1.jar                                \
    help-$fourFiveFourVersion.jar                  \
    log4j-1.2.16.jar                               \
    rochesynth-$fourFiveFourVersion.jar            \
    validation-2.1.0.jar                           \
    )

libDir="$rootDir"/lib
CPATH="$rootDir"
CPATH="$CPATH":"$configDir"
CPATH="$CPATH":"$rootDir"/bin
CPATH="$CPATH":"$rootDir"/../../bin
CPATH="$CPATH":"$libDir"
CPATH="$CPATH":"$rootDir"/doc
CPATH="$CPATH":"$rootDir"/video

for jarFile in "${jarFiles[@]}"
do
	CPATH="$CPATH":"$libDir"/$jarFile
done

export CPATH

# Main Class, Documentation and Logger Config
mainClass=com.fourfivefour.assembly.AssemblyMain
docDir=-Dhelp.base.file="$rootDir"/doc
loggerConfig=gsAssembler.log4j.properties

# New Project Initialization options
newProjectInitScript=newProjectInit.ava
scriptLibDir="$configDir"/lib

defineLogger=-Dlog4j.configuration="$loggerConfig"

# Launch the java application
stdJavaArgs=("$defineLogger"                    \
             "$docDir"                          \
             -Djava.awt.headless=$enableHeadless\
             -mx${heapMem}M                     \
             -XX:MaxPermSize=${permMem}m        \
             -cp "$CPATH"                       \
             $mainClass                         \
             $enableVersion                     \
             $enableVerbose                     \
            )

finalJavaArgs=("${stdJavaArgs[@]}")

if [ "$enableFeatures" != "" ]; then
	finalJavaArgs=("-Dcom.fourfivefour.enable=$enableFeatures" "${finalJavaArgs[@]}")
fi

if [ "$remoteX" != "" ]; then
	finalJavaArgs=("-Dsun.java2d.pmoffscreen=false" "${finalJavaArgs[@]}")
fi

# Assume a single jre subdirectory always.
JRE_LOC=$(find "$rootDir"/apps/jre/ -name "jre1.6*" | head -1)
export JAVA_HOME="$JRE_LOC"
export JAVA="$JAVA_HOME"/bin/java

"$JAVA" "${finalJavaArgs[@]}" $heapMem $permMem 
