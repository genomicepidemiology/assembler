#!/usr/bin/env python3
import platform
import os

###   SET SYSTEM PATH   ###
def setSystem():
   '''Identifies architecture and sets system specific
      variables such as executable paths'''

   plat = platform.uname()

   sys = {}
   sys['home'] = os.path.dirname(os.path.realpath(__file__))
   sys['bin_home'] = '%s/bin/'%(sys['home'])
   sys['velvet_home'] = '%s/velvet/'%(sys['bin_home'])
   sys['newbler'] = '%s/454/bin/'%(sys['bin_home'])
   sys['solid_home'] = '%s/solid/'%(sys['bin_home'])

   return(sys)

def rm_files(patterns):
   '''Remove files using glob given as list of patterns'''
   import glob
   import os

   for p in patterns:
      files = glob.glob(p)
      if len(files) > 0:
         for f in files:
            os.remove(f)

def set_filetype(f):
   '''Detects filetype from fa, fq and sff input file'''
   import gzip

   if f.endswith('.gz'):
      inhandle = gzip.open(f, 'rt')
   else:
      inhandle = open(f, "rt")

   line = inhandle.readline()
   if line.startswith(">"):
      out = 'fasta'
   elif line.startswith("@"):
      out = 'fastq'
   else:
      inhandle = open(f, "rt")
      line = inhandle.readline()
      if line.startswith(".sff"):
         out = 'sff'
      else:
         raise ValueError('Input must be fasta, fastq or sff')

   return out

def set_fqtype(f):
   '''Detects sanger or illumina format from fastq'''
   from Bio.SeqIO.QualityIO import FastqGeneralIterator
   import re
   import gzip
   # Open fastq, convert ASCII to number, check if number is above or below certain thresholds to determine format

   # type is (header encoding, quality encoding)
   type = ['nd', 'nd']

   if f.endswith('.gz'):
      inhandle = gzip.open(f, 'rt')
   else:
      inhandle = open(f, 'rt')

   re_illumina = re.compile('^\w+-?\w+:\w+:\w+:\w+:\w+#.+\/\d')
   re_illumina_v18 = re.compile('^\w+-\w+:\w+:\w+:\d+:\d+:\d+:\d+\s\d:\w:\d+:\w+')
   re_illumina_sra = re.compile('^\w+\.\d+\s.+\/\d$')
   re_illumina_miseq = re.compile('^\w+:\d+:\d+-\w+:\d+:\d+:\d+:\d+\s\d:\w:\d+:\w+')
   re_illumina_miseq2 = re.compile('^\w+:\d+:\w+:\d+:\d+:\d+:\d+\s\d:\w:\d+:\w+')
   re_illumina_miseq3 = re.compile('^\w+-\w+:\d+:\w+-\w+:\d+:\d+:\d+:\d+\s\d:\w:\d+:\w+')

   for (title, sequence, quality) in FastqGeneralIterator(inhandle):
      # detect header encoding
      hd = re_illumina.match(title)
      if hd:
         type[0] = 'Illumina1.4'
      else:
         hd = re_illumina_v18.match(title)
         if hd:
            type[0] = 'Illumina1.8'
         else:
            hd = re_illumina_sra.match(title)
            if hd: type[0] = 'IlluminaSRA'
            else:
               hd = re_illumina_miseq.match(title)
               if hd: type[0] = 'Illumina1.8'
               else:
                  hd = re_illumina_miseq2.match(title)
                  if hd: type[0] = 'Illumina1.8'
                  else:
                     hd = re_illumina_miseq3.match(title)
                     if hd: type[0] = 'Illumina1.8'

      # detect quality encoding
      for q in map(ord, quality):
         if q > 83:
            type[1] = 'Illumina'
            break
         elif q < 59:
            type[1] = 'Sanger'
            break
      if type[1] != 'nd':
         break

   if type[1] == 'nd':
      raise ValueError('Fastq format not identified, are you sure it is sanger/illumina?')
   else:
      return type
