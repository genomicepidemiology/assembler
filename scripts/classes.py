#!/usr/bin/env python3
import sys, os, logging, random, re, stat, string
from subprocess import Popen, PIPE, check_output, CalledProcessError
from time import sleep
import modules

home = os.getcwd()
paths = modules.setSystem()

class Moab:
   '''Submits a list of calls to the queue using qsub. Job dependencies are controlled using depend (logical), depend_type ('one2one', 'expand', 'conc', 'complex', 'all'), depend_val (list of integers) and ids (list of jobids):
         
         'one2one' makes job 1 dependent on id 1, job 2 on id 2 ...
         'expand' makes n first jobs dependent on id 1, next n jobs dependent on id 2 ...
         'conc' makes job 1 dependent on n first ids, job 2 dependent on next n ids ...
         'complex' takes several integers in a list as input and makes the jobs dependent on the number of ids given. Eg.[2,1] means that the first job will be dependent on the first 2 ids and the second job will be dependent on the 3 id ...
      
      Jobs are submitted as hold by default and should be released using Moab.release().
   '''
   def __init__(self, calls, logfile=None, runname='run_test', queue=None, cpu='nodes=1:ppn=1,mem=2gb,walltime=43200', depend=False, depend_type='one2one', depend_val=[], hold=True, depend_ids=[], env=None, host=None):
      '''Constructor for Moab class'''
      self.calls = calls
      self.runname = runname
      self.logfile = logfile
      self.queue = queue
      self.cpu = cpu
      self.depend = depend
      self.depend_type = depend_type
      self.depend_val = depend_val
      self.hold = hold
      self.depend_ids = depend_ids
      self.env = env
      self.host = host
      
      # put jobs in queue upon construction
      self.dispatch()
   
   def __repr__(self):
      '''Return string of attributes'''
      msg = 'Moab(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)' % ("["+", ".join(self.calls)+"]", self.logfile, self.runname, self.queue,  self.cpu, str(self.depend), self.depend_type, "["+", ".join(map(str,self.depend_val))+"]", str(self.hold), "["+", ".join(self.depend_ids)+"]", self.env, self.host)
      return msg
   
   def get_logger(self):
      '''Return logger object'''
      if self.logfile:
         # if already a logger do nothing and return
         # else create logger with given logfile
         if isinstance(self.logfile, logging.Logger):
            return self.logfile
         else:
            logger = logging.getLogger('moab_submissions')
            hdlr = logging.FileHandler('%s' % self.logfile)
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)
            logger.addHandler(hdlr) 
            logger.setLevel(logging.INFO)
            return logger
      else:
         return None
   
   def create_dependencies(self):
      '''Create job dependencies
         
         'one2one' makes job 1 dependent on id 1, job 2 on id 2 ...
         'expand' makes n first jobs dependent on id 1, next n jobs dependent on id 2 ...
         'conc' makes job 1 dependent on n first ids, job 2 dependent on next n ids ...
         'complex' takes a n=list as input and makes the jobs dependent on the number of ids given in n. Eg.[2,1] means that the first job will be dependent on the first 2 ids and the second job will be dependent on the 3 id ...
      '''
      if not self.depend:
         depends = None
      else:
         if self.depend_type == 'one2one':
            depends = self.depend_ids
         
         elif self.depend_type == 'expand':
            n = int(self.depend_val[0])
            depends = []
            depend_ids = self.depend_ids     # do not want to remove from self.depend_ids because it will remove the ids from input class instance (eg. other calls)
            c = 0
            for j in range(len(self.calls)):
               c = c + 1
               if c < int(n):
                  depends.append(depend_ids[0])
               if c == int(n):
                  depends.append(depend_ids[0])
                  depend_ids = depend_ids[1:]
                  c = 0
         
         elif self.depend_type == 'conc':
            n = int(self.depend_val[0])
            depends = []
            depend_ids = self.depend_ids     # do not want to remove from self.depend_ids because it will remove the ids from input class
            for j in range(len(self.calls)):
               s = ':'.join(depend_ids[:int(n)])
               depends.append(s)
               depend_ids = depend_ids[int(n):]
         
         elif self.depend_type == 'complex':
            old_index = 0
            depends = []
            for index in self.depend_val:
               s = ':'.join(self.depend_ids[old_index:(index+old_index)])
               depends.append(s)
               old_index=index
         
         elif self.depend_type == 'all':
            depends = []
            for j in range(len(self.calls)):
               curr = ':'.join(self.depend_ids)
               depends.append(curr)
         
         else:
            raise AttributeError('depend_type not recognized: %s' % self.depend_type)
      return depends
   
   def ssh_submit(self, cmd):
      '''Will submit commands and return job ids'''
      p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
      stdout, stderr = p.communicate()
      id = ''
      for line in stdout.split('\n'):
         id = line.rstrip('\n')
      
      err_msg = stderr
      
      return id, err_msg
   
   def submit_qsub(self, depends, logger):
      '''Submits jobs using qsub'''
      home = os.getcwd()
      paths = modules.setSystem()
      
      ids = []
      for i in range(len(self.calls)):
         call = self.calls[i]
         stdout = '%s/log/%s%i.o' % (home, self.runname, i)
         stderr = '%s/log/%s%i.e' % (home, self.runname, i)
         
         # catch stdouts if call includes 'program infile > outfile', needs to be directed as -O instead of >
         pattern = re.compile(r'(^.+)>\s(.+)$')
         match = pattern.search(call)
         if match:
            call = match.group(1)
            stdout = '%s/%s' % (home, match.group(2))
         
         # create qsub commands
         cmd = 'qsub -h'
         
         # toggle if job should be on hold or env variable should be added
         if self.env: cmd += ' -v %s'%self.env
         
         # Set queue
         if self.queue: cmd += ' -q %s'%self.queue
         
         # Set dependencies
         if self.depend: cmd += ' -W depend=afterany:%s'%depends[i]
         
         qsub = '%s -d %s -l %s -o %s -e %s -r y -N %s %s'%(cmd, home, self.cpu, stdout, stderr, self.runname, home+'/'+call)
         
         if logger: logger.info(qsub)
         
         # submit on different host if that is given
         if self.host:
            try:
               (id, stderr) = self.ssh_submit(qsub)
            except:
               print(stderr)
               print('Job error, waiting 10s')
               sleep(10)
               (id, stderr) = self.ssh_submit(qsub)
            ids.append(id.split('.')[0])
         else:
            try:
               id = check_output(qsub, shell=True)
            except:
               print('Job error, waiting 10s')
               sleep(10)
               id = check_output(qsub, shell=True)
            ids.append(id.split('.')[0])
         
         # Take a break between job submissions to avoid overloading the queue
         sleep(1)
      
      return ids
   
   def submit_wrapcmd(self, depends, logger):
      '''Take input as command and submit to qsub (this way pipes and redirects can be done)'''
      home = os.getcwd()
      paths = modules.setSystem()
      
      ids = []
      for i in range(len(self.calls)):
         call = self.calls[i]
         N = 10
         rand = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(N))
         filename = '%s/pbsjob.tmp%s' % (home, rand)
         
         stdout = '%s/log/%s%i.o' % (home, self.runname, i)
         stderr = '%s/log/%s%i.e' % (home, self.runname, i)
         
         # write pbsjob file
         fh = open(filename, 'w')
         fh.write('#!/bin/sh\n\n')
         fh.write('%s\n' % call)
         fh.close()
         # Set execution permission
         st = os.stat(filename)
         os.chmod(filename, st.st_mode | stat.S_IXUSR)
         
         # create qsub command
         cmd = 'qsub -h'
         
         # toggle if job should be on hold or env variable should be added
         if self.env: cmd += ' -v %s'%self.env
         
         # Set queue
         if self.queue: cmd += ' -q %s'%self.queue
         
         # Set dependencies
         if self.depend: cmd += ' -W depend=afterany:%s'%depends[i]
         
         qsub = '%s -d %s -l %s -o %s -e %s -r y -N %s %s'%(cmd, home, self.cpu, stdout, stderr, self.runname, filename)
         
         if logger: logger.info(qsub)
         
         # submit on different host if that is given
         if self.host:
            try:
               (id, stderr) = self.ssh_submit(qsub)
            except:
               print(stderr)
               print('Job error, waiting 10s')
               sleep(10)
               (id, stderr) = self.ssh_submit(qsub)
            ids.append(id.split('.')[0])
         else:
            try:
               id = check_output(qsub, shell=True)
            except:
               print('Job error, waiting 10s')
               sleep(10)
               id = check_output(qsub, shell=True)
            ids.append(id.split('.')[0])
         
         # Take a break between job submissions to avoid overloading the queue
         sleep(1)
      
      return ids
   
   def dispatch(self):
      '''Submit job to queue, if depend=True dependent jobids should be given as ids. 
         Dependtype can be 'one2one', 'expand' 'conc', 'complex'.
         
         'one2one' makes job 1 dependent on id 1, job 2 on id 2 ...
         'expand' makes n first jobs dependent on id 1, next n jobs dependent on id 2 ...
         'conc' makes job 1 dependent on n first ids, job 2 dependent on next n ids ...
         'complex' takes a n=list as input and makes the jobs dependent on the number of ids given in n. Eg.[2,1] means that the first job will be dependent on the first 2 ids and the second job will be dependent on the 3 id ...
      '''
      # start logger
      logger = self.get_logger()
      
      # create job dependencies
      depends = self.create_dependencies()
      
      if self.calls[0].find('|') > -1 or self.calls[0].find('<') > -1 or self.calls[0].find('>>') > -1 or self.calls[0].find(';') > -1:
         # perform wrapcmd if calls includes pipes / left-redirects
         self.ids = self.submit_wrapcmd(depends, logger)
      else:
         # perform qsub if calls does not include pipes (can have right-redirects)
         self.ids = self.submit_qsub(depends, logger)
      
      # Give the queuing system a break to register the jobs
      sleep(30)
   
   def release(self):
      '''Release submitted jobs from hold'''
      for id in self.ids:
         # Release Job
         cmd = 'mjobctl -u user %s'%(id)
         tries_left = 20
         while tries_left:
            try:
               out = check_output(cmd, shell=True)
            except CalledProcessError:
               tries_left -= 1
               if tries_left:
                  sleep(10)
               else:
                  raise
            else:
               break
         
         # Take a break between jobs to distribute the load on the machine(s)
         sleep(15)

class Semaphore_file:
   '''Wait for a file to appear'''
   def __init__(self, semaphore_file, check_interval, max_time):
      '''Constructor for Semaphore_file class'''
      self.semaphore_file = semaphore_file
      self.check_interval = check_interval
      self.max_time = max_time
   
   def wait(self):
      '''Wait for files to be created'''
      # add directory and set semaphore filename
      if not os.path.exists('semaphores/'):
         os.makedirs('semaphores/')
      
      # check for file to appear
      cnt = self.max_time
      while cnt > 0:
         if os.path.isfile(self.semaphore_file):
            break
         cnt -= self.check_interval
         sleep(self.check_interval)
      
      if cnt <= 0:
         raise SystemExit('Error: %s did not finish in %is\n' % (self.semaphore_file, self.max_time))
