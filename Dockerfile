############################################################
# Dockerfile to build the CGE Assembler
############################################################

# Load base Docker image
FROM python:3.5.3-jessie

# Disable Debian frontend interaction promts
ENV DEBIAN_FRONTEND noninteractive

# Update the repository sources list
RUN set -ex; \
    apt-get update; \
    apt-get install -y apt-utils \
        aufs-tools \
        automake \
        build-essential \
        ca-certificates \
        debian-archive-keyring \
        dpkg-sig \
        iptables \
        libapparmor-dev \
        libcap-dev \
        libmysqlclient-dev \
        libsqlite3-dev \
        lxc \
        openssh-server \
        pigz \
        python3-pip \
        r-base \
    ; \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/*;

# Re-enable Debian frontend interaction promts
ENV DEBIAN_FRONTEND Teletype

# Install Python dependencies
RUN pip3 install -U pip biopython

# Install Assembler
RUN mkdir /usr/src/assembler
ADD . /usr/src/assembler

# Set entrypoint
ENTRYPOINT ["/usr/src/assembler/scripts/wrapper_denovo_2.0.py"]
CMD ["--help"]

# Set default working directory
WORKDIR /workdir
